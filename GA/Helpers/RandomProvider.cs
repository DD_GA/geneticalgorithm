﻿using System;

namespace GA.Helpers
{
    public static class RandomProvider
    {
        public static Random Current { get; } = new Random();
    }
}
