﻿using GA.Abstracts;
using GA.BasicTypes;
using GA.Helpers;

namespace GA.Implementations
{
    public class UniformMutationOperator : IMutationOperator
    {
        public void Mutation(Individual individual, double mutationProbability, int currentGeneration, int maxGenerations)
        {
            var random = RandomProvider.Current;

            for (int i = 0; i < individual.Chromosome.Size; i++)
            {
                individual.Chromosome.Genes[i].Value += random.NextDouble() < 0.5 ? 1 : -1;
            }
        }
    }
}
