﻿using GA.Abstracts;
using System;
using System.Linq;
using GA.BasicTypes;
using GA.Helpers;

namespace GA.Implementations
{
    public class RouletteWheelSelection : ISelectionOperator
    {
        public void CalculateDistribuance(Individual[] currentPopulation, out double[] distribuance)
        {
            var sumOfFitness = currentPopulation
                .Sum(x => x.Fitness);

            double cummulativeFitness = 0;
            distribuance = currentPopulation
                .Select(x => cummulativeFitness += x.Fitness / sumOfFitness)
                .ToArray();
        }

        public Individual[] GenerateParentPopulation(Individual[] currentPopulation)
        {
            CalculateDistribuance(currentPopulation, out var distribuance);

            return currentPopulation.Select(x => SelectOne(currentPopulation, distribuance)).Select(x => x.Clone()).ToArray();
        }

        private Individual SelectOne(Individual[] currentPopulation, double[] distribuance)
        {
            double randomNumber = RandomProvider.Current.NextDouble();

            double total = 0.0;
            for (int i = 0; i < distribuance.Length; ++i)
            {
                if (randomNumber >= total)
                {
                    return currentPopulation[i];
                }
                total += distribuance[i];
            }

            return currentPopulation.Last();
        }
    }
}
