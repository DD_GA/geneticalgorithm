﻿using System;
using System.Linq;

namespace GA.Implementations
{
    public class FloatingDecoder : IValueDecoder
    {
        private readonly double _minValue;
        private readonly double _maxValue;

        public FloatingDecoder(double minValue, double maxValue)
        {
            _minValue = minValue;
            _maxValue = maxValue;
        }

        public double Decode(bool[] genes)
        {
            return _minValue + (_maxValue - _minValue) * GetDecodedValue(genes) / Math.Pow(2.0, genes.Length);
        }

        private double GetDecodedValue(bool[] genes)
        {
            return genes
                .Reverse()
                .Select((x, i) => x ? Math.Pow(2, i) : 0)
                .Sum();
        }
    }
}
