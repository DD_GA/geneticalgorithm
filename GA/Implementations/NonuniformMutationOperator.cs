﻿using System;
using GA.Abstracts;
using GA.BasicTypes;
using GA.Helpers;

namespace GA.Implementations
{
    public class NonuniformMutationOperator : IMutationOperator
    {
        private readonly double _c;

        public NonuniformMutationOperator(double c)
        {
            _c = c;
        }

        public void Mutation(Individual individual, double mutationProbability, int currentGeneration, int maxGenerations)
        {
            var random = RandomProvider.Current;

            for (int i = 0; i < individual.Chromosome.Size; i++)
            {
                if (random.NextDouble() < mutationProbability)
                {
                    individual.Chromosome.Genes[i] = MutateGene(
                        currentGeneration, 
                        maxGenerations,
                        individual.Chromosome.Genes[i], 
                        random);
                }
            }
        }

        private Gene MutateGene(int currentGeneration, int maxGenerations, Gene gene, Random random)
        {
            return new Gene
            {
                Min = gene.Min,
                Max = gene.Max,
                Value = random.NextDouble() < 0.5
                    ? gene + Delta(currentGeneration, maxGenerations, random.NextDouble(), gene.Max - gene)
                    : gene - Delta(currentGeneration, maxGenerations, random.NextDouble(), gene - gene.Min)
            };
        }

        private double Delta(double t, double T, double r, double y)
        {
            return y * (1.0 - Math.Pow(r, (1.0 - t / T) * _c));
        }
    }
}
