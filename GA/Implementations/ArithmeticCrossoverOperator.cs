using System.Linq;
using GA.Abstracts;
using GA.BasicTypes;
using GA.Helpers;

namespace GA.Implementations
{
    public class ArithmeticCrossoverOperator : ICrossOperator
    {
        public void Crossover(Individual x1, Individual x2)
        { 
            var random = RandomProvider.Current;
            var k = random.NextDouble();
            var chromosomeSize = x1.Chromosome.Size;
            
            var y = x1.Chromosome.Genes
                .Zip(x2.Chromosome.Genes, (x1i, x2i) => x1i + k * (x2i - x1i))
                .ToArray();
            
            var z = Enumerable.Range(0, chromosomeSize)
                .Select(i => x2.Chromosome.Genes[i] + x1.Chromosome.Genes[i] - y[i])
                .ToArray();

            x1.Chromosome.Genes = y.Zip(x1.Chromosome.Genes, (yi, xi) => xi.NewGene(yi)).ToArray();
            x2.Chromosome.Genes = z.Zip(x2.Chromosome.Genes, (zi, xi) => xi.NewGene(zi)).ToArray();
        }
    }
}