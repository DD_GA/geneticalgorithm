﻿using System;
using System.Linq;

namespace GA.Implementations
{
    public class IntegerDecoder : IValueDecoder
    {
        public double Decode(bool[] genes)
        {
            return GetDecodedValue(genes);
        }

        private double GetDecodedValue(bool[] genes)
        {
            return genes
                .Reverse()
                .Select((x, i) => x ? Math.Pow(2, i) : 0)
                .Sum();
        }
    }
}
