﻿using System;
using System.Collections.Generic;
using System.Linq;
using GA.Abstracts;
using GA.BasicTypes;
using GA.Helpers;

namespace GA.Implementations
{
    public class TournamentSelectionOperator : ISelectionOperator
    {
        private readonly int _tournamentSize;

        public TournamentSelectionOperator(int tournamentSize)
        {
            _tournamentSize = tournamentSize;
        }

        public Individual[] GenerateParentPopulation(Individual[] currentPopulation)
        {
            var random = RandomProvider.Current;

            return currentPopulation
                .Select(x => SelectOne(currentPopulation, random))
                .ToArray();
        }

        private Individual SelectOne(Individual[] currentPopulation, Random random)
        {
            var pool = currentPopulation.ToList();
            var randomIndividuals = new List<Individual>();
            for (int i = 0; i < _tournamentSize; ++i)
            {
                var randomIndividual = pool[random.Next(0, pool.Count)];
                pool.Remove(randomIndividual);
                randomIndividuals.Add(randomIndividual);
            }

            return randomIndividuals
                .OrderByDescending(y => y.Fitness)
                .First()
                .Clone();
        }
    }
}
