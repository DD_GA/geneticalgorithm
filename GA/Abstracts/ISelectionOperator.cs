﻿using GA.BasicTypes;

namespace GA.Abstracts
{
    public interface ISelectionOperator
    {
        Individual[] GenerateParentPopulation(Individual[] currentPopulation);
    }
}