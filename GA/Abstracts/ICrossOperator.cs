using GA.BasicTypes;

namespace GA.Abstracts
{
    public interface ICrossOperator
    {
        void Crossover(Individual x1, Individual x2);
    }
}