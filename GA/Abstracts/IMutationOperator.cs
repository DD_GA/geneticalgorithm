using GA.BasicTypes;

namespace GA.Abstracts
{
    public interface IMutationOperator
    {
        void Mutation(Individual individual, double mutationProbability, int currentGeneration, int maxGenerations);
    }
}