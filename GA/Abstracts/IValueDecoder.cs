﻿namespace GA
{
    public interface IValueDecoder
    {
        double Decode(bool[] genes);
    }
}