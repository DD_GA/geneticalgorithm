using System;

namespace GA.BasicTypes
{
    public class GeneInfo
    {
        private readonly double _minValue;
        private readonly double _maxValue;

        public GeneInfo(double minValue, double maxValue)
        {
            _minValue = minValue;
            _maxValue = maxValue;
        }

        public Gene CreateGene(Random random)
        {
            return new Gene
            {
                Min = _minValue,
                Max = _maxValue,
                Value = _minValue + (_maxValue - _minValue) * random.NextDouble()
            };
        }
    }
}