using System.Linq;
using GA.Helpers;

namespace GA.BasicTypes
{
    public class ChromosomeType
    {
        private readonly GeneInfo[] _geneInfos;
        public int Size => Genes.Length;

        public Gene this[int index]
        {
            get => Genes[index];
            set => Genes[index] = value;
        }

        public Gene[] Genes { get; set; }

        public ChromosomeType(GeneInfo[] geneInfos)
        {
            _geneInfos = geneInfos;

            var random = RandomProvider.Current;
            Genes = geneInfos
                .Select(x => x.CreateGene(random))
                .ToArray();
        }
        
        public ChromosomeType Clone()
        {
            return new ChromosomeType(_geneInfos)
            {
                Genes = Genes.Select(x => x).ToArray()
            };
        }
    }
}
