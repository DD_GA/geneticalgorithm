﻿using System.Globalization;

namespace GA.BasicTypes
{
    public struct Gene
    {
        public double Min;
        public double Max;
        public double Value;
        
        public static implicit operator double(Gene gene)
        {
            return gene.Value;
        }

        public Gene NewGene(double newValue)
        {
            return new Gene {Min = Min, Max = Max, Value = newValue};
        }

        public override string ToString()
        {
            return Value.ToString(CultureInfo.InvariantCulture);
        }
    }
}
