using System.Linq;

namespace GA.BasicTypes
{
    public class Individual
    {
        private readonly GeneInfo[] _geneInfos;
        public ChromosomeType Chromosome { get; set; }
        public double Fitness { get; set; }

        public Individual(GeneInfo[] geneInfos)
        {
            _geneInfos = geneInfos;
            Chromosome = new ChromosomeType(geneInfos);
        }

        public void UpdateFitness(FitnessFunction fitness)
        {
            Fitness = fitness(Chromosome.Genes.Select(x => x.Value).ToArray());
        }
        
        public Individual Clone()
        {
            return new Individual(_geneInfos)
            {
                Chromosome = Chromosome.Clone()
            };
        }

        public override string ToString()
        {
            return $"({string.Join(" ", Chromosome.Genes)})";
        }
    }
}
