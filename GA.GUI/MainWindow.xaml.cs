﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using WPFSurfacePlot3D;
using GA.Implementations;
using GA.BasicTypes;

namespace GA.GUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        public SurfacePlotModel PlotViewModel { get; }

        public MainWindow()
        {
            InitializeComponent();
            PlotViewModel = new SurfacePlotModel();
            UpdatePlot();
            DataContext = this;
        }

        public double MinX { get; set; } = 0;
        public double MaxX { get; set; } = Math.PI;
        public double MinY { get; set; } = 0;
        public double MaxY { get; set; } = Math.PI;

        public void OnMinXChanged() => UpdatePlot();
        public void OnMaxXChanged() => UpdatePlot();
        public void OnMinYChanged() => UpdatePlot();
        public void OnMaxYChanged() => UpdatePlot();

        private void UpdatePlot()
        {
            PlotViewModel.PlotFunction(Func, MinX, MaxX, MinY, MaxY);
        }

        private double Func(double x, double y)
        {
            return -(Math.Pow(x, 2) + Math.Pow(y, 2));
        }

        #region Events
        private void HandleSomethingHappened(int i, Individual bestIndividual)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                Log.Text += $"Iteration: {i} Fitness: {bestIndividual.Fitness} Genes: {string.Join(", ", bestIndividual.Chromosome.Genes)}\n";
            });
        }
        private async void RunSimulationAction(object sender, RoutedEventArgs e)
        {
            var ga = new GeneticAlgorithm(
                   5,
                   new ArithmeticCrossoverOperator(),
                   new NonuniformMutationOperator(2),
                   new TournamentSelectionOperator(3),
                   x => Func(x[0], x[1]),
                   new GeneInfo(MinX, MaxX),
                   new GeneInfo(MinY, MaxY));
            ga.SomethingHappened += HandleSomethingHappened;

            await Task.Run(() => ga.RunSimulation(1000));
        }
        #endregion

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
