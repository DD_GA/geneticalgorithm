﻿using GA.BasicTypes;
using GA.Implementations;
using System;

namespace GA
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                var ga = new GeneticAlgorithm(
                    5,
                    new ArithmeticCrossoverOperator(),
                    new NonuniformMutationOperator(2),
                    new TournamentSelectionOperator(3),
                    x => Math.Pow(x[0], 2) + Math.Pow(x[1], 2),
                    new GeneInfo(0, Math.PI),
                    new GeneInfo(0, Math.PI));
                ga.RunSimulation(10000);
                Console.ReadKey();
            }
        }
    }
}
