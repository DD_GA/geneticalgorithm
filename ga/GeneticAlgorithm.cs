using System;
using System.Linq;
using GA.Abstracts;
using GA.BasicTypes;

namespace GA
{
    public delegate double FitnessFunction(params double[] x);
    public delegate void LogPossibility(int i, Individual bestIndividual);

    public class GeneticAlgorithm
    {
        #region Fields
        private static Random _random = new Random();
        private readonly ICrossOperator _crossOperator;
        private readonly IMutationOperator _mutationOperator;
        private readonly ISelectionOperator _selectionOperator;
        private Individual[] _population;
        private readonly int _numberOfIndividuals;
        private readonly FitnessFunction _fitnessFunction;
        private readonly GeneInfo[] _geneInfos;
        #endregion

        #region Properties
        public double CrossoverProbability { get; set; }
        public double MutationProbability { get; set; }
        #endregion

        public event LogPossibility SomethingHappened;

        public GeneticAlgorithm(
            int numberOfIndividuals,
            ICrossOperator crossOperator,
            IMutationOperator mutationOperator,
            ISelectionOperator selectionOperator,
            FitnessFunction fitnessFunction,
            params GeneInfo[] geneInfos)
        {
            _numberOfIndividuals = numberOfIndividuals;
            _crossOperator = crossOperator;
            _mutationOperator = mutationOperator;
            _selectionOperator = selectionOperator;
            _fitnessFunction = fitnessFunction;
            _geneInfos = geneInfos;
            CrossoverProbability = 0.90;
            MutationProbability = 0.05;
        }
        
        public Individual RunSimulation(int maxNumberOfGenerations)
        {
            ResetSimulations();

            Individual bestIndividual = null;

            for (int i = 0; i < maxNumberOfGenerations; i++)
            {
                var parents = _selectionOperator.GenerateParentPopulation(_population);

                for (int j = 0; j < _numberOfIndividuals - 1; j += 2)
                {
                    if (_random.NextDouble() < CrossoverProbability)
                    {
                        _crossOperator.Crossover(parents[j], parents[j + 1]);
                        
                        _mutationOperator.Mutation(parents[j], MutationProbability, i, maxNumberOfGenerations);
                        _mutationOperator.Mutation(parents[j + 1], MutationProbability, i, maxNumberOfGenerations);
                    }
                }

                _population = parents;
                UpdateFitness();

                bestIndividual = _population
                    .OrderByDescending(x => x.Fitness)
                    .First();

                SomethingHappened?.Invoke(i, bestIndividual);
                //Console.WriteLine($"Iteration: {i} Fitness: {bestIndividual.Fitness} Genes: {string.Join(", ", bestIndividual.Chromosome.Genes)}");
            }

            return bestIndividual;
        }

        private void UpdateFitness()
        {
            foreach (var individual in _population)
            {
                individual.UpdateFitness(_fitnessFunction);
            }
        }

        private void ResetSimulations()
        {
            _population = new Individual[_numberOfIndividuals];
            for (int i = 0; i < _numberOfIndividuals; i++)
            {
                _population[i] = new Individual(_geneInfos);
                _population[i].UpdateFitness(_fitnessFunction);
            }
        }
    }
}